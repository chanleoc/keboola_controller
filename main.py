"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'keboola_controller'"

"""
Python 3 environment 
"""

#import pip
#pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'logging_gelf'])

import sys
import os
import logging
import csv
import json
import requests
import pandas as pd
import logging_gelf.formatters
import logging_gelf.handlers
from keboola import docker
import urllib.parse as url_parse
import uuid


### Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

### Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

logger = logging.getLogger('gelf')
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])


### Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
storage_token = cfg.get_parameters()["#storage_token"]
name = cfg.get_parameters()["name"]
row_name = cfg.get_parameters()["row_name"]

### Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
out_tables = cfg.get_expected_output_tables()
logging.info("IN tables mapped: "+str(in_tables))
logging.info("OUT tables mapped: "+str(out_tables))

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"


def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))
    
    return in_name

def get_output_tables(out_tables):
    """
    Evaluate output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = out_tables[0]
    in_name = table["full_path"]
    in_destination = table["source"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name

def transformation_flow(transformation_name, row_name,input_df):
    """
    Process Flow for creating Transformation
    """
    found, transformation_id = check_transformation(transformation_name)

    if not found:
        transformation_id = create_transformation(transformation_name)

    create_transformation_row(row_name ,transformation_id, input_df)

    logging.info("Transformation {0} Created.".format(transformation_name))
    logging.info("Transformation Creation Flow Completed.")

def check_transformation(transformation_name):
    """
    Check if the exisiting Transformation name is used in the current project
    Return: Boolean if transformation found, transaction_id if found
    """

    logging.info("Entered Transformation Value: {0}".format(transformation_name))

    component_id = "transformation"
    url = "https://connection.keboola.com/v2/storage/components/{0}/configs?isDeleted=false".format(component_id)
    headers = {
        "X-StorageApi-Token": str(storage_token)
    }

    r = requests.get(url, headers=headers)
    results = json.loads(r.text)

    ### Parameters
    #test_subject = "gdpr_testing"
    transformation_id = ""
    found = 0

    for transformation in results:
        if transformation["name"]==transformation_name:
            transformation_id = transformation["id"]
            logging.info("Transformation Name Found; Transformation ID: {0}".format(transformation_id))
            found += 1
    
    logging.info("Transformation Found: {0}".format(found))
    if found > 1:
        logging.info("Since there are multiple transformation with the same name. Application will create another transformation with the same name.")
        return False, ""
    elif found == 1:
        return True, transformation_id
    else:
        return False, ""

def create_transformation(transformation_name):
    """
    Creating transformation in KBC
    Return: transaction_id of the new transformation
    """

    ### Transformation Creation Parameters
    component_id = "transformation"
    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-StorageApi-Token": str(storage_token)
    }
    config_name = {
        "name": str(transformation_name)
    }

    ### Create Transformation Configuration
    url = "https://connection.keboola.com/v2/storage/components/{0}/configs".format(component_id)
    logging.info("Transformation Creation URL: {0}".format(url))
    r = requests.post(url, headers=headers, data=config_name)
    results = json.loads(r.text)
    logging.info("Transformation Creation Status: {0}".format(r.status_code))
    logging.info("Transformation Creation Result: {0}".format(results))
    
    transaction_id = results["id"]
    
    return transaction_id

def create_transformation_row(row_name, transaction_id, input_df):
    """
    Create Transformation Configuration Rows
    Return: Status of the API request
    """

    url = "https://connection.keboola.com/v2/storage/components/{0}/configs/{1}/rows".format("transformation", transaction_id)
    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-StorageApi-Token": str(storage_token)
    }
    logging.info("Transformation Row Creation URL: {0}".format(url))
    
    row_id = uuid.uuid4()
    
    inputs = _create_row_input(input_df)
    row_config = {
        "id": str(row_id),
        "input": inputs,
        "backend": "snowflake",
        "type": "simple",
        "phase": 1,
        "disabled": False,
        "description": "Transformation created from Keboola Controller"
    }

    logging.info("Transfomration Row Configuration: {0}".format(row_config))
    request_config = "configuration="+url_parse.quote_plus(json.dumps(row_config))
    request_name = "name="+row_name
    request_rowid = "rowId="+str(row_id)
    full_request_config = request_config+"&"+request_name+"&"+request_rowid
    logging.info("Transformation Row Creation Converted body: {0}".format(full_request_config))

    r = requests.post(url, headers=headers, data=full_request_config)
    results = json.loads(r.text)

    logging.info("Transformation Row Creation Status: {0}".format(r.status_code))
    logging.info("Transformation Row Creation Results: {0}".format(results))
    
def _create_row_input(df):
    """
    Subset of create_transformation to determine the input mappings
    Return: An array with the input configurations
    """

    input_table = []
    ### First 
    ### filter the list of tables needed to be imported into the transformation
    table_names = df["table"].unique()
    for table_name in table_names:
        ### Second
        ### Filter the list of columns and values needed to be imported into the transformation
        df_filtered = df.loc[df["table"]==table_name]
        column_names = df_filtered["column_name"].unique()
        
        for column_name in column_names:
            ### Iterating with the unique set of columns     
            df_filtered_2 = df_filtered.loc[df_filtered["column_name"]==column_name]

            cell_values = df_filtered_2["cell_value"].unique()
            temp_input = {
                "source": table_name,
                "destination": table_name.split(".")[2]+"_"+column_name,
                "whereColumn": column_name,
                "whereValues": cell_values.tolist(),
                "whereOperator": "eq"
            }

            input_table.append(temp_input)
    
    return input_table


def main():
    """
    Main execution script.
    """

    in_table = get_tables(in_tables)
    data_in = pd.read_csv(in_table, dtype=str)
    
    ### Transformation Creation
    transformation_flow(name, row_name, data_in)

    return


if __name__ == "__main__":

    main()

    logging.info("Done.")
