# Keboola Controller

### Current Functionality
1. Transformation Creation
    - Features in creating transformations with pre-configured input mapping and input mapping filters
    - Required Files: a file containing all the source of the file within the same project and resources to filter for the input mapping
    - Required UI Configuration:
        1. Storage Token
            - The required token for the application to access your exisiting project
        2. Name
            - The name of the transformation to be created
        3. Row Name
            - The row name of the transformation
    - Required Files' Columns: 
        1. table
            - The source of the table to be inserted into the input mapping
        2. column_name
            - Column of the input file which will be used to match up the cell_value
        3. cell_value
            - The filter value which will be used filtered out the rows from the input mapping

Docker Version: 0.0.1